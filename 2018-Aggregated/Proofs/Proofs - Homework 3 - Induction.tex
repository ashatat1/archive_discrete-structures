\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Proofs: Mathematical Induction}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 2.3}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 2.4}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}

    \begin{intro}{Review: Rules of exponents}
        \begin{itemize}
            \item   Power Rule: $(a^{m})^{n} = a^{mn}$
            \item   Negative Exponent Rule: $a^{-n} = \frac{1}{a^{n}}$
            \item   Product Rule: $a^{m} \cdot a^{n} = a^{m+n}$
            \item   Quotient Rule: $\frac{a^{m}}{a^{n}} = a^{m-n}$
        \end{itemize}
    \end{intro}

% ASSIGNMENT ------------------------------------ %

    % ------------------------------------------------------------------ CHAPTER -- %
    \newpage
    \subsection*{Recursive / Closed formula equivalence}

    \begin{intro}{Example} \small
        Show that the sequence defined by the \textbf{recursive formula}
        \footnote{From Discrete Mathematics, Ensley and Crawley} ~\\
        $$a_{k} = a_{k-1} + 4; a_{1} = 1$$ for $k \geq 2$
        is equivalently described by the \textbf{closed formula} $$a_{n} = 4n - 3$$

        \hrulefill
        
        \paragraph{Basis Step: Check $a_{1}$ for both formulas.} ~\\ ~\\
            Recursive: $a_{1} = 1$ (provided); \tab
            Closed: $a_{1} = 4(1) - 3 = 1$  \tab \checkmark OK

        \hrulefill

        \paragraph{\small Inductive Step: Show that this is true for all values up through $n-1$:}

        \subparagraph{\footnotesize Find an equation for $a_{k-1}$ via the closed formula provided:} ~\\

        Original proposition:   $a_{n} = 4n - 3$ and $a_{k} = a_{k-1} + 4; a_{1} = 1$ are equivalent. Use $a_{n} = 4n - 3$ to find a value for $a_{k-1}$. ~\\

        \begin{tabular}{p{5cm} l}
            1. $a_{n} = 4n - 3$         & The closed formula   \\
            2. $a_{k-1} = 4(k-1) - 3$   & Plugging in $k-1$ into $n$ \\
            3. $a_{k-1} = 4k - 4 - 3$   & Simplifying... \\
            4. $a_{k-1} = 4k - 7$       & Simplified.
        \end{tabular}

        \subparagraph{\footnotesize Plug the equation for $a_{k-1}$ into the recursive formula and simplify.} ~\\
        
        \begin{tabular}{p{5cm} l}
            1. $a_{k} = a_{k-1} + 4$                    & The recursive formula \\
            2. $a_{k} = 4k - 7 + 4$                     & Plugging in $a_{k-1} = 4k - 7$. \\
            3. \framebox[1.1\width]{$a_{k} = 4k - 3$}   & Simplified to the original form.
        \end{tabular} ~\\~\\

        We have manipulated the \textbf{recursive formula} to end up with the
        same \textbf{closed formula} as stated in the original proposition,
        therefore we have shown that they are equivalent.

    \end{intro}
    \normalsize

    \newpage
    \begin{intro}{Example} \footnotesize
    
        Use induction to prove the proposition. As part of the proof,
        verify the statement for $n = 1$, $n = 2$, and $n = 3$.
        $\sum_{i=1}^{n} (2i - 1) = n^{2} $        for each $n \geq 1$. \footnote{From Discrete Mathematics by Ensley and Crawley}

        \hrulefill

        \paragraph{Basis step: Show that the proposition is true for 1, 2, and 3. } ~\\

            \begin{tabular}{l l l} 
                \scriptsize $i=1$: & \scriptsize LHS: $ \sum_{i=1}^{1} (2i - 1) = ( 2 \cdot 1 - 1 ) = 1$ & \scriptsize RHS: $1^{2} = 1$ \checkmark
                \\
                \scriptsize $i=2$: & \scriptsize LHS: $ \sum_{i=1}^{2} (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = (1) + (3) = 4 $ & \scriptsize RHS: $2^{2} = 4$ \checkmark
                \\
                \scriptsize $i=3$: & \scriptsize LHS: $\sum_{i=1}^{3} (2i - 1) = 1 + 3 + (2 \cdot 3 - 1) = 1 + 3 + 5 = 9 $ & \scriptsize RHS: $3^{2} = 9$ \checkmark
            \end{tabular} \footnote{LHS = left-hand side, RHS = right-hand side} ~\\

        \hrulefill
        
        \paragraph{Inductive Step} ~\\
        \subparagraph{The sum is equivalent to the sum up until $n-1$, plus the final term at $i=n$:} ~\\

        \begin{tabular}{p{7cm} p{4.2cm}}
            1. $\sum_{i=1}^{n}(2i-1)$ & The original sum. \\ \\
            2. $\sum_{i=1}^{n}(2i-1) = \sum_{i=1}^{n-1}(2i-1) + (2n - 1)$ & \footnotesize The sum is equivalent to the sum from $i=1$ to $n-1$, plus the final $i=n$. \\
        \end{tabular}

        \subparagraph{Find an equation for $\sum_{i=1}^{n-1}$ from the original proposition:} ~\\
        
        \begin{tabular}{p{7cm} p{4cm}}
            1. $\sum_{i=1}^{n} (2i - 1) = n^{2}$ & Original proposition. \\
            2. $\sum_{i=1}^{n-1} (2i - 1) = (n-1)^{2}$ & Plugging in $n-1$. \\
            3. $\sum_{i=1}^{n-1} (2i - 1) = n^{2} - 2n + 1$ & Simplified. \\
        \end{tabular}

        \subparagraph{Plug $\sum_{i=1}^{n-1}$ into the equation for the sum made previously:} ~\\
        
        \begin{tabular}{p{7cm} p{4cm}}
            1. $\sum_{i=1}^{n}(2i-1) = \sum_{i=1}^{n-1}(2i-1) + (2n - 1)$ & Our sum formula. \\
            2. $\sum_{i=1}^{n}(2i-1) = (n^{2} - 2n + 1) + (2n - 1)$ & plugged in $\sum_{i=1}^{n-1} (2i - 1) = n^{2} - 2n + 1$. \\
            3. \framebox[1.1\width]{$\sum_{i=1}^{n}(2i-1) = n^{2}$} & Simplified to the original proposition. \\
        \end{tabular} ~\\

        We get the same form as the original proposition, proving our statement.
    \end{intro}

    \newpage
    \begin{enumerate}
        \item   Use induction to prove that each sum is equivalent to each formula for each $n \geq 1$   \footnote{Discrete Mathematics, Ensley and Crawley, p 122}
            \begin{itemize}
                \item[a.]   $$\sum_{i=1}^{n} (2i-1) = n^{2}$$
                \item[b.]   $$\sum_{i=1}^{n} (2i+4) = n^{2} + 5n $$
                \item[c.]   $$\sum_{i=1}^{n} (2^{i} - 1) = 2^{n+1} - n - 2 $$
                %\item[d.]   $$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = 1 - \frac{1}{2^{n}} $$
                %\item[d.]   $$\sum_{i=1}^{n} (\frac{1}{i(i+1)}) = \frac{n}{n+1} $$
                %\item[e.]   $$\sum_{i=1}^{n} (\frac{1}{(2i-1)(2i+1)}) = \frac{n}{2n+1} $$
            \end{itemize}
    
        \item   Use induction to prove that each pair of recursive formulas and closed formulas are equivalent to each other. \footnote{Discrete Mathematics, Ensley and Crawley, p 121}
            \begin{itemize}
                \item[a.]   Recursive: $a_{k} = a_{k-1} + 4$, $a_{1} = 1$ \tab for $k \geq 2$ ~\\
                            Closed: $a_{n} = 4n-3$.
                            
                \item[b.]   Recursive: $a_{k} = a_{k-1} + (k+4)$, $a_{1} = 5$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = \frac{n(n+9)}{2}$.
                            
                \item[c.]   Recursive: $a_{k} = a_{k-1} + k^{2}$, $a_{1} = 1$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = \frac{n(n+1)(2n+1)}{6}$.
                            
                \item[d.]   Recursive: $a_{k} = 2 a_{k-1} + 1$, $a_{1} = 1$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = 2^{n} - 1$.
            \end{itemize}
    \end{enumerate}


    
    \newpage
    \begin{answer}
    \section*{Mathematical Induction - Answer key}
    
    \footnotesize
    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   $$\sum_{k=1}^{n} (2k-1) = n^{2}$$  ~\\~\\
                            Basis Step: Check for $n=1$, $n=2$, $n=3$: ~\\ ~\\
                            \begin{tabular}{l p{7cm} | p{2cm} l}
                                $n$ & Summation & Equation \\
                                $n=1$
                                    & $\sum_{k=1}^{1} (2k-1) = (2 \cdot 1 - 1) = 1$
                                    & $1^{2} = 1$
                                    & \checkmark \\
                                $n=2$
                                    & $\sum_{k=1}^{2} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = 1 + 3 = 4$
                                    & $2^{2} = 4$
                                    & \checkmark \\
                                $n=3$
                                    & $\sum_{k=1}^{3} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) + (2 \cdot 3 - 1) = 1 + 3 + 5 = 9$
                                    & $3^{2} = 9$
                                    & \checkmark
                            \end{tabular} ~\\~\\

                            Inductive Steps: ~\\
                            \begin{itemize}
                                \item   Rewrite the sum as the sum of all items from $k=1$ to $n-1$, plus the final term $2n-1$. ~\\
                                        $$\sum_{k=1}^{n} (2k-1) \equiv \sum_{k=1}^{n-1} (2k-1) + (2n-1)$$
                                \item   We need an equation for $\sum_{k=1}^{n-1} (2k-1)$, so we use the original proposition here. ~\\
                                        $\sum_{k=1}^{n} (2k-1) = n^{2}$ ... $\sum_{k=1}^{n-1} (2k-1) = (n-1)^{2}$ ...
                                        $$\sum_{k=1}^{n-1} (2k-1) = n^{2} - 2n + 1$$
                                \item   Plug the equation for $\sum_{k=1}^{n-1} (2k-1)$ back into the equation for $\sum_{k=1}^{n} (2k-1)$ and simplify: ~\\
                                        $\sum_{k=1}^{n} (2k-1) = \sum_{k=1}^{n-1} (2k-1) + (2n-1)$ ~\\
                                        $\sum_{k=1}^{n} (2k-1) = n^{2} - 2n + 1 + (2n-1)$ ~\\
                                        $\sum_{k=1}^{n} (2k-1) = n^{2}$ ~\\
                                        \begin{framed}$$\sum_{k=1}^{n} (2k-1) = n^{2}$$\end{framed} ~\\
                                        This is the same form as the original proposition. Therefore, we have proven it.
                                        
                            \end{itemize}

                \newpage
                \item[b.]   $$\sum_{i=1}^{n} (2i+4) = n^{2} + 5n $$ ~\\~\\
                            Basis Step: ~\\~\\
                            \begin{tabular}{l p{5cm} | p{3cm} l}
                                $n$ & Summation & Equation \\
                                $n=1$
                                    & $\sum_{k=1}^{1} (2i+4) = (2 \cdot 1 + 4) = 6$
                                    & $1^{2} + 5(1) = 6$
                                    & \checkmark \\
                                $n=2$
                                    & $\sum_{k=1}^{2} (2i+4) = (2 \cdot 1 + 4) + (2 \cdot 2 + 4) = 6 + 8 = 14$
                                    & $2^{2} + 5(2) = 14$
                                    & \checkmark \\
                                $n=3$
                                    & $\sum_{k=1}^{3} (2i+4) = (2 \cdot 1 + 4) + (2 \cdot 2 + 4) + (2 \cdot 3 + 4) = 6 + 8 + 10 = 24$
                                    & $3^{2} + 5(3) = 24$
                                    & \checkmark
                            \end{tabular} ~\\~\\
                            ~\\~\\
                            Inductive Steps:
                            \begin{itemize}
                                \item   Rewrite sum: ~\\
                                        $$\sum_{k=1}^{n} (2i+4) \equiv \sum_{k=1}^{n-1} (2i+4) + (2n+4)$$
                                \item   Find equation for sum from 1 to $n-1$: ~\\
                                        $\sum_{i=1}^{n-1} (2i+4) = (n-1)^{2} + 5(n-1) $ ~\\
                                        $\sum_{i=1}^{n-1} (2i+4) = n^{2} - 2n + 1 + 5n - 5 $ ~\\
                                        $\sum_{i=1}^{n-1} (2i+4) = n^{2} + 3n - 4$ ~\\
                                        
                                \item   Plug into sum equation: ~\\
                                        $\sum_{k=1}^{n} (2i+4) = \sum_{k=1}^{n-1} (2i+4) + (2n+4)$ ~\\
                                        $\sum_{k=1}^{n} (2i+4) = n^{2} + 3n - 4 + (2n+4)$ ~\\
                                        $\sum_{k=1}^{n} (2i+4) = n^{2} + 5n$ ~\\
                                        \begin{framed}$$\sum_{k=1}^{n} (2i+4) = n^{2} + 5n$$\end{framed} ~\\
                                        We got it back to the original form, so we have proven the proposition.
                            \end{itemize}

                \newpage
                \item[c.]   $$\sum_{i=1}^{n} (2^{i} - 1) = 2^{n+1} - n - 2 $$ ~\\~\\
                            Basis Step: ~\\~\\
                            \begin{tabular}{l p{7cm} | p{2cm} l}
                                $n$ & Summation & Equation \\
                                $n=1$
                                    & $\sum_{k=1}^{1} (2^{i} - 1) = (2^{1} - 1) = 1$
                                    & $2^{1+1} - 1 - 2 = 4 - 1 - 2 = 1$
                                    & \checkmark \\
                                $n=2$
                                    & $\sum_{k=1}^{2} (2^{i} - 1) = (2^{1} - 1) + (2^{2} - 1) = 1 + 3 = 4$
                                    & $2^{2+1} - 2 - 2 = 8 - 2 - 2 = 4$
                                    & \checkmark \\
                                $n=3$
                                    & $\sum_{k=1}^{3} (2^{i} - 1) = (2^{1} - 1) + (2^{2} - 1) + (2^{3} - 1) = 1 + 3 + 7 = 11$
                                    & $2^{3+1} - 3 - 2 = 16 - 3 - 2 = 11$
                                    & \checkmark
                            \end{tabular} ~\\~\\
                            ~\\~\\
                            Inductive Steps:
                            \begin{itemize}
                                \item   Rewrite sum: ~\\
                                        $$ \sum_{i=1}^{n} (2^{i} - 1) \equiv \sum_{i=1}^{n-1} (2^{i} - 1) + (2^{n} - 1) $$
                                \item   Find sum from 1 to $n-1$: ~\\
                                        $ \sum_{i=1}^{n-1} (2^{i} - 1) = 2^{n-1+1} - (n-1) - 2 $ ~\\
                                        $ \sum_{i=1}^{n-1} (2^{i} - 1) = 2^{n} - n + 1 - 2 $ ~\\
                                        $ \sum_{i=1}^{n-1} (2^{i} - 1) = 2^{n} - n - 1 $ ~\\
                                \item   Plug into sum equation: ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = \sum_{i=1}^{n-1} (2^{i} - 1) + (2^{n} - 1)$ ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = 2^{n} - n - 1 + (2^{n} - 1)$ ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = 2^{n} + 2^{n} - n - 1 - 1$ ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = 2(2^{n}) - n - 2$ ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = (2^{1})(2^{n}) - n - 2$ ~\\
                                        $\sum_{i=1}^{n} (2^{i} - 1) = 2^{n+1} - n - 2$ ~\\
                                        \begin{framed}$$\sum_{i=1}^{n} (2^{i} - 1) = 2^{n+1} - n - 2$$\end{framed} ~\\

                                        For this one, I had to add two items with like terms, $2^{n} + 2^{n}$.
                                        If we had $a + a$, the result would be $2a$, so here instead the result
                                        is $2( 2^{n} )$. Then, using the product rule, that gives us $2^{1} \cdot 2^{n}$
                                        and then $2^{n+1}$.
                            \end{itemize}
                        \end{itemize}

                %\newpage
                %\item[d.]   $$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = 1 - \frac{1}{2^{n}} $$ ~\\~\\
                            %Basis Step: ~\\~\\
                            %\begin{tabular}{l p{7cm} | p{2cm} l}
                                %Summation & Equation \\
                                %$n=1$
                                    %& $\sum_{i=1}^{1} (\frac{1}{2^{i}}) = \frac{1}{2^{1}} = \frac{1}{2}$
                                    %& $1^{2} = 1$
                                    %& \checkmark \\
                                %$n=2$
                                    %& $\sum_{i=1}^{2} (\frac{1}{2^{i}}) = \frac{1}{2^{1}} + \frac{1}{2^{2}} = \frac{1}{2} + \frac{1}{4} = \frac{3}{4}$
                                    %& $2^{2} = 4$
                                    %& \checkmark \\
                                %$n=3$
                                    %& $\sum_{i=1}^{3} (\frac{1}{2^{i}}) = \frac{1}{2^{1}} + \frac{1}{2^{2}} + \frac{1}{2^{3}} = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} = \frac{7}{8}$
                                    %& $3^{2} = 9$
                                    %& \checkmark
                            %\end{tabular} ~\\~\\
                            %~\\~\\
                            %Inductive Steps:
                            %\begin{itemize}
                                %\item   Rewrite sum: ~\\
                                        %$$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = \sum_{i=1}^{n-1} (\frac{1}{2^{i}}) + (\frac{1}{2^{n}})$$
                                %\item   Find sum from 1 to $n-1$: ~\\
                                        %$$\sum_{i=1}^{n-1} (\frac{1}{2^{i}}) = 1 - \frac{1}{2^{n-1}} $$ ~\\
                                %\item   Plug into sum equation: ~\\
                                        %$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = \sum_{i=1}^{n-1} (\frac{1}{2^{i}}) + (\frac{1}{2^{n}})$ ~\\
                                        %$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = \frac{1}{2^{n-1}} + (\frac{1}{2^{n}})$ ~\\
                                        %$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = (\frac{2^{n}}{2^{n}}) \frac{1}{2^{n-1}} + (\frac{1}{2^{n}}) (\frac{2^{n-1}}{2^{n-1}})$ ~\\
                                        %$\sum_{i=1}^{n} (\frac{1}{2^{i}}) = \frac{2^{n} + 2^{n-1}}{(2^{n-1})(2^{n})}$ ~\\
                                        %...
                            %\end{itemize}

                %\newpage
                %\item[d.]   $$\sum_{i=1}^{n} (\frac{1}{i(i+1)}) = \frac{n}{n+1} $$ ~\\~\\
                            %Basis Step: ~\\~\\
                            %\begin{tabular}{l p{7cm} | p{2cm} l}
                                %Summation & Equation \\
                                %$n=1$
                                    %& $\sum_{k=1}^{1} (2k-1) = (2 \cdot 1 - 1) = 1$
                                    %& $1^{2} = 1$
                                    %& \checkmark \\
                                %$n=2$
                                    %& $\sum_{k=1}^{2} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = 1 + 3 = 4$
                                    %& $2^{2} = 4$
                                    %& \checkmark \\
                                %$n=3$
                                    %& $\sum_{k=1}^{3} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) + (2 \cdot 3 - 1) = 1 + 3 + 5 = 9$
                                    %& $3^{2} = 9$
                                    %& \checkmark
                            %\end{tabular} ~\\~\\
                            %~\\~\\
                            %Inductive Steps:
                            %\begin{itemize}
                                %\item   Rewrite sum: ~\\
                                %\item   Find sum from 1 to $n-1$: ~\\
                                %\item   Plug into sum equation: ~\\
                            %\end{itemize}

                %\newpage
                %\item[e.]   $$\sum_{i=1}^{n} (\frac{1}{(2i-1)(2i+1)}) = \frac{n}{2n+1} $$ ~\\~\\
                            %Basis Step: ~\\~\\
                            %\begin{tabular}{l p{7cm} | p{2cm} l}
                                %Summation & Equation \\
                                %$n=1$
                                    %& $\sum_{k=1}^{1} (2k-1) = (2 \cdot 1 - 1) = 1$
                                    %& $1^{2} = 1$
                                    %& \checkmark \\
                                %$n=2$
                                    %& $\sum_{k=1}^{2} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = 1 + 3 = 4$
                                    %& $2^{2} = 4$
                                    %& \checkmark \\
                                %$n=3$
                                    %& $\sum_{k=1}^{3} (2k-1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) + (2 \cdot 3 - 1) = 1 + 3 + 5 = 9$
                                    %& $3^{2} = 9$
                                    %& \checkmark
                            %\end{tabular} ~\\~\\
                            %~\\~\\
                            %Inductive Steps:
                            %\begin{itemize}
                                %\item   Rewrite sum: ~\\
                                %\item   Find sum from 1 to $n-1$: ~\\
                                %\item   Plug into sum equation: ~\\
                            %\end{itemize}
            %\end{itemize}
            
        \item
            \begin{itemize}
                \item[a.]   Recursive: $a_{k} = a_{k-1} + 4$, $a_{1} = 1$ \tab for $k \geq 2$ ~\\
                            Closed: $a_{n} = 4n-3$. ~\\

                            \paragraph{Basis step:} ~\\~\\
                            Recursive: $a_{1} = 1$, Closed: $a_{1} = 1$ \checkmark

                            \paragraph{Inductive steps:} ~\\~\\
                            Find equation for $a_{k-1}$: ~\\ $a_{n} = 4n-3$ \tab $a_{k-1} = 4(k-1) - 3$, \tab $a_{k-1} = 4k - 7$ ~\\

                            Plug into recursive: ~\\
                            $a_{k} = a_{k-1} + 4$, \tab $a_{k} = 4k - 7 + 4$, \tab \framebox[1.1\width]{$a_{k} = 4k - 3$}

                \hrulefill
                \item[b.]   Recursive: $a_{k} = a_{k-1} + (k+4)$, $a_{1} = 5$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = \frac{n(n+9)}{2}$.

                            \paragraph{Basis step:} ~\\~\\
                            Recursive: $a_{1} = 5$, Closed: $a_{1} = \frac{1(10)}{2} = 5$ \checkmark

                            \paragraph{Inductive steps:} ~\\~\\
                            Find equation for $a_{k-1}$: ~\\ $a_{n} = \frac{n(n+9)}{2}$ \tab $a_{k-1} = \frac{(k-1)(k-1+9)}{2}$,
                            \tab $a_{k-1} = \frac{(k-1)(k+8)}{2}$ ~\\~\\
                            $a_{k-1} = \frac{k^{2} + 7k - 8}{2}$
                            ~\\

                            Plug into recursive: ~\\
                            $a_{k}  = a_{k-1} + (k+4)$, \tab $a_{k} = \frac{k^{2} -7k - 8}{2} + (k+4)$, \tab
                            $a_{k} = \frac{k^{2} + 7k - 8}{2} + \frac{2(k+4)}{2}$ ~\\~\\
                            $a_{k} = \frac{k^{2} + 7k - 8 + 2k + 8}{2}$  \tab
                            $a_{k} = \frac{k^{2} + 9k}{2}$  \tab
                            \framebox[1.1\width]{$a_{k} = \frac{k(k + 9)}{2}$}

                \newpage                            
                \item[c.]   Recursive: $a_{k} = a_{k-1} + k^{2}$, $a_{1} = 1$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = \frac{n(n+1)(2n+1)}{6}$.

                            \paragraph{Basis step:} ~\\~\\
                            Recursive: $a_{1} = 1$, Closed: $a_{1} = \frac{1(1+1)(2 \cdot 1 + 1)}{6} = \frac{1(2)(3)}{6} = 1$ \checkmark

                            \paragraph{Inductive steps:} ~\\~\\
                            Find equation for $a_{k-1}$: ~\\ $a_{n} = \frac{n(n+1)(2n+1)}{6}$
                            \tab $a_{k-1} = \frac{(k-1)(k-1+1)(2(k-1)+1)}{6}$,
                            ~\\~\\ $a_{k-1} = \frac{(k-1)(k)(2k-1)}{6}$
                            \tab $a_{k-1} = \frac{(k^{2} - k)(2k-1)}{6}$
                            ~\\~\\ $a_{k-1} = \frac{2k^{3} - k^{2} - 2k^{2} + k}{6}$
                            \tab $a_{k-1} = \frac{2k^{3} - 3k^{2} + k}{6}$ ~\\

                            Plug into recursive: ~\\
                            $a_{k} = a_{k-1} + k^{2}$,
                            \tab $a_{k} = \frac{2k^{3} - 3k^{2} + k}{6} + k^{2}$,
                            \tab $a_{k} = \frac{2k^{3} - 3k^{2} + k}{6} + \frac{6k^{2}}{6}$,
                            ~\\~\\ $a_{k} = \frac{2k^{3} - 3k^{2} + k + 6k^{2}}{6}$,
                            \tab $a_{k} = \frac{2k^{3} + 3k^{2} + k}{6}$,
                            \tab $a_{k} = \frac{k(2k^{2} + 3k^{1} + 1)}{6}$,
                            ~\\~\\ $a_{k} = \frac{k(2k^{2} + 3k + 1)}{6}$,
                            \tab \framebox[1.1\width]{$a_{k} = \frac{k(2k + 1)(k+1)}{6}$}

                \hrulefill                            
                \item[d.]   Recursive: $a_{k} = 2 a_{k-1} + 1$, $a_{1} = 1$ \tab for $k \geq 2$
                            ~\\ Closed: $a_{n} = 2^{n} - 1$.

                            \begin{center}
                                NOTE: Product Rule: $a^{m} \cdot a^{n} = a^{m+n}$
                            \end{center}

                            \paragraph{Basis step:} ~\\~\\
                            Recursive: $a_{1} = 1$, Closed: $a_{1} = 2^{1} - 1 = 1$ \checkmark

                            \paragraph{Inductive steps:} ~\\~\\
                            Find equation for $a_{k-1}$: ~\\ $a_{n} = 2^{n} - 1$
                            \tab $a_{k-1} = $ ~\\

                            Plug into recursive: ~\\
                            $a_{k} = 2 a_{k-1} + 1$,
                            \tab $a_{k} = 2 (2^{k-1} - 1) + 1$,
                            ~\\ $a_{k} = 2^{1} \cdot 2^{k} \cdot 2^{-1} - 2 + 1$,
                            \tab $a_{k} = \frac{2}{2} \cdot 2^{k} - 1$,
                            ~\\ \framebox[1.1\width]{$a_{k} = \cdot 2^{k} - 1$}
            \end{itemize}

    \end{enumerate}
    \end{answer}
    
\input{../BASE-4-FOOT}
