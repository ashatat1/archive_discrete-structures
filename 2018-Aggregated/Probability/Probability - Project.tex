\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laChapter}     {Combinatorics Project}
\newcommand{\laTitle}       {Combinatorics Project}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 6}
%\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter ?}
\newcommand{\laTextbookB}   {}
\addtocounter{section}{5}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-PROJECTS}

% ASSIGNMENT ------------------------------------ %

    \section*{Combinatorics Project}

    \subsection*{Formulas}

        \paragraph{Probability in a Bernoulli Trial:}

        Given a simple experiment, called a \textbf{Bernoulli trial},
        and an event that occurs with a probability $p$, if the trial is repeated
        independently $n$ times, then the probability of having exactly
        $k$ successes is
        \footnote{From Discrete Math by Ensley and Crawley, page 460}
        $$ C(n, k) \cdot p^{k} \cdot (1 - p)^{n-k} $$

        \paragraph{Expected (Average) Value:}

        For a given probability experiment, let $X$ be a random
        variable whose possible values come from the set of numbers
        $ x_{1}, ..., x_{n} $. Then the \textbf{expected value of $X$},
        denoted by $E[X]$, is the sum

        $$ (x_{1}) \cdot Prob(X = x_{1}) + (x_{2}) \cdot Prob(X = x_{2}) + ... + (x_{n}) \cdot Prob(X = x_{n}) $$


    \newpage
    \subsection*{Program: Calculating the Expected Average}

        For this program, you will have the user enter information about the
        trial we're looking at. You will compute the \textbf{expected average, $E[X]$}.

        You should have a menu in the program like this:
        
\texttt{ ~\\
 1.	Enter game info                 ~\\
 2.	Calculate expected average      ~\\
 3.	Quit
}

        ~\\ Keep running the program until the user decides to quit.

        \paragraph{Part 1: Getting info about the trials} ~\\

        \textbf{To help illustrate the data, let's assume the user will
        be entering data for the 3rd set of example data - tossing 3 coins.}

        \begin{center}
            \begin{tabular}{ p{0.5cm} | p{1cm} | p{8cm} | p{1cm} | p{1cm} }
                \tiny   Index   &
                \tiny   Value   &
                \tiny   Events  &
                \tiny   Event amt &
                \tiny   Probability
                \\
                $i$ &
                $x_{i}$ &
                $E_{i}$ &
                $n(E_{i})$ &
                $Prob(E_{i})$
                \\ \hline
                1 & 3 & \{ (H, H, H) \} & 1 & $1/8$
                \\
                2 & 2 & \{ (H, H, T), (H, T, H), (T, H, H) \} & 3 & $3/8$
                \\
                3 & 1 & \{ (H, T, T), (T, H, T), (T, T, H) \} & 3 & $3/8$
                \\
                4 & 0 & \{ (T, T, T) \} & 1 & $1/8$
            \end{tabular}
        \end{center}

        You will need to ask the user for $n(S)$ first. In this example,
        since there are three coins to toss, the total size of the sample set is $2^{3} = 8$.

\begin{lstlisting}[style=output]
What is the size of the sample set, n(S)?
>> 8
\end{lstlisting} ~\\

        Next, we need to know how many game outcomes there are. This is different
        from the sample size. In this case, each Heads gives us +1 point,
        so there are four outcomes: 3 points, 2 points, 1 point, and 0 points.
 
\begin{lstlisting}[style=output]
How many game value outcomes are there?
(How many x[i] items?)
>> 4
\end{lstlisting} ~\\

        Then, loop to get all the possible game score outcomes there are.
        These should be stored in some sort of list structure.
        
\begin{lstlisting}[style=output]
x[1] = ?
>> 3
\end{lstlisting} ~\\

        We need to store the probability as well, but it would be
        better to get the event set size $n(E_{i})$ and then
        compute the probability on the program side.

\begin{lstlisting}[style=output]
How many events lead to this score?
>> 1
\end{lstlisting} ~\\

        Probability of getting this value $x_{1}$ will be $n(E_{1}) / n(S)$.
        Store the probability in a list structure as well.
        
\begin{lstlisting}[style=output]
Probability calculated as 0.125%
\end{lstlisting} ~\\

        After entering all the game info, it would be useful to display
        the table of data on the main menu like this:
        
\begin{lstlisting}[style=output]
Current game data:

Outcome #      Score value         Probability         
0              3                   0.125%               
1              2                   0.375%               
2              1                   0.375%               
3              0                   0.125%   
\end{lstlisting} ~\\
        
        \paragraph{Part 2: Computing the expected average}

        For this one, you will need a variable to store the expected value.
        The expected value is calculated like this:

        $$E[X] = \sum_{i=1}^{6} (x_{i} \cdot Prob(X=x_{i})$$

        So you will use a loop to get a running sum.
    
        \newpage
        For example...

\begin{lstlisting}[style=code]
float expectedValue = 0;
for ( int i = 0; i < totalScoreOutcomes; i++ )
{
    expectedValue += values[i] * probability[i];
}
\end{lstlisting} ~\\

        Once the loop is completed, you will have $E[X]$. Display it to the screen.
        
\begin{lstlisting}[style=output]
Calculated expected value is:  1.5
\end{lstlisting} ~\\

        Test out the program with several sets of data. The calculations
        are given in the following three examples.


    \newpage
    \subsubsection*{Example data}

        Use these examples to test your program output.

        \paragraph{Rolling 1 die:} ~\\

        When rolling one die, the sample size, $n(S)$, is 6.
        There are 6 total outcomes. In this case, the outcome values
        match the die roll value, but this isn't always true in all
        these examples provided.

        \begin{center}
            \begin{tabular}{ p{2cm} | p{4cm} | p{2cm} | p{4cm} }
                \footnotesize Value of outcome &
                \footnotesize Set of events that give the outcome &
                \footnotesize Amount of events that result in the outcome value &
                \footnotesize Probability of event
                \\
                 $x_{i}$ &
                 $E_{i}$ &
                 $n(E_{i})$ &
                 $Prob(E_{i}) = \frac{n(E_{i})}{n(S)}$
                \\ \hline
                1 & \{ 1 \} & 1 & 1/6
                \\
                2 & \{ 2 \} & 1 & 1/6
                \\
                3 & \{ 3 \} & 1 & 1/6
                \\
                4 & \{ 4 \} & 1 & 1/6
                \\
                5 & \{ 5 \} & 1 & 1/6
                \\
                6 & \{ 6 \} & 1 & 1/6
            \end{tabular}
        \end{center}

        Expected value calculation: \\
        $$E[X] = \sum_{i=1}^{6} (x_{i} \cdot Prob(X=x_{i})$$
        ~\\~\\ $\tab = x_{1} \cdot Prob(X=x_{1}) + x_{2} \cdot Prob(X=x_{2}) + x_{3} \cdot Prob(X=x_{3}) + x_{4} \cdot Prob(X=x_{4}) + x_{5} \cdot Prob(X=x_{5}) + x_{6} \cdot Prob(X=x_{6})$
        ~\\~\\ $\tab = 1(\frac{1}{6}) + 2(\frac{1}{6}) + 3(\frac{1}{6}) + 4(\frac{1}{6}) + 5(\frac{1}{6}) + 6(\frac{1}{6})$
        ~\\~\\ $\tab = \frac{1 + 2 + 3 + 4 + 5 + 6}{6}$ \tab $= \frac{21}{6}$ \tab $= 3.5$

        \begin{center}
            \textbf{The expected value calculated is 3.5.}
        \end{center}

        \newpage
        \paragraph{Rolling 2 dice and summing their values for the outcomes:} ~\\

        We are rolling two dice, so there are $n(S) = 36$ total outcomes that can happen.
        Since we're summing the values, we will have values of 2 through 12 possible (1+1 up to 6+6).

        \begin{center}
            \begin{tabular}{ p{0.5cm} | p{1cm} | p{8cm} | p{1cm} | p{1cm} }
                \tiny   Index   &
                \tiny   Value   &
                \tiny   Events  &
                \tiny   Event amt &
                \tiny   Probability
                \\
                $i$ &
                $x_{i}$ &
                $E_{i}$ &
                $n(E_{i})$ &
                $Prob(E_{i})$
                \\ \hline
                    \footnotesize 1
                & 2 & \{ (1, 1) \}                                            & 1 & 1/36
                \\  \footnotesize 2
                & 3 & \{ (1, 2), (2, 1) \}                                    & 2 & 2/36
                \\  \footnotesize 3
                & 4 & \{ (1, 3), (2, 2), (3, 1) \}                            & 3 & 3/36
                \\  \footnotesize 4
                & 5 & \{ (1, 4), (2, 3), (3, 2), (4, 1) \}                    & 4 & 4/36
                \\  \footnotesize 5
                & 6 & \{ (1, 5), (2, 4), (3, 3), (4, 2), (5, 1) \}            & 5 & 5/36
                \\  \footnotesize 6
                & 7 & \{ (1, 6), (2, 5), (3, 4), (4, 3), (5, 2), (6, 1) \}    & 6 & 6/36
                \\  \footnotesize 7
                & 8 & \{ (2, 6), (3, 5), (4, 4), (5, 3), (6, 2) \}            & 5 & 5/36
                \\  \footnotesize 8
                & 9 & \{ (3, 6), (4, 5), (5, 4), (6, 3) \}                    & 4 & 4/36
                \\  \footnotesize 9
                & 10 & \{ (4, 6), (5, 5), (6, 4) \}                           & 3 & 3/36
                \\  \footnotesize 10
                & 11 & \{ (5, 6), (6, 5) \}                                   & 2 & 2/36
                \\  \footnotesize 11
                & 12 & \{ (6, 6) \}                                           & 1 & 1/36
            \end{tabular}
        \end{center}

        Expected value calculation: \\
        $$E[X] = \sum_{i=1}^{11} (x_{i} \cdot Prob(X=x_{i})$$

        ~\\~\\ $\tab = 2(\frac{1}{36}) + 3(\frac{2}{36}) + 4(\frac{3}{36}) + 5(\frac{4}{36}) + 6(\frac{5}{36}) + 7(\frac{6}{36}) + 8(\frac{5}{36}) + 9(\frac{4}{36}) + 10(\frac{3}{36}) + 11(\frac{2}{36}) + 12(\frac{1}{36})$
        ~\\
        
        $$ = \frac{2 + 6 + 12 + 20 + 30 + 42 + 40 + 36 + 30 + 22 + 12}{36} $$
        
        $$ = \frac{252}{36} \tab = 7$$

        \begin{center}
            \textbf{The expected value calculated is 7.}
        \end{center}


        \newpage
        \paragraph{Flipping 3 coins, giving a point for heads:} ~\\

        In this game, we flip 3 coins. For each HEADS we get, +1 point is gained.
        Otherwise, 0 points are gained for tails. For this example, $n(S) = 8$
        and there will be 4 total possible values:

        \begin{center}
            \begin{tabular}{ p{0.5cm} | p{1cm} | p{8cm} | p{1cm} | p{1cm} }
                \tiny   Index   &
                \tiny   Value   &
                \tiny   Events  &
                \tiny   Event amt &
                \tiny   Probability
                \\
                $i$ &
                $x_{i}$ &
                $E_{i}$ &
                $n(E_{i})$ &
                $Prob(E_{i})$
                \\ \hline
                1 & 3 & \{ (H, H, H) \} & 1 & $1/8$
                \\
                2 & 2 & \{ (H, H, T), (H, T, H), (T, H, H) \} & 3 & $3/8$
                \\
                3 & 1 & \{ (H, T, T), (T, H, T), (T, T, H) \} & 3 & $3/8$
                \\
                4 & 0 & \{ (T, T, T) \} & 1 & $1/8$
            \end{tabular}
        \end{center}

        Expected value calculation: \\
        $$E[X] = \sum_{i=1}^{4} (x_{i} \cdot Prob(X=x_{i})$$

        $$ = 3(\frac{1}{8}) + 2(\frac{3}{8}) + 1(\frac{3}{8}) + 0(\frac{1}{8})$$

        $$ = \frac{3 + 6 + 3 + 0}{8} \tab = \frac{12}{8} \tab = 1.5$$ 
        
        \begin{center}
            \textbf{The expected value calculated is 1.5.}
        \end{center}

    \newpage
    \subsubsection*{Example output}

        \paragraph{Main menu:} ~\\

\begin{lstlisting}[style=output]
-----------------------------------------------------
 | Main Menu |
 -------------

Current game data:

Outcome #      Score value         Probability         


 1.	Enter game info
 2.	Calculate expected average
 3.	Quit

 >> 1

\end{lstlisting}

        \paragraph{Getting game info:} ~\\

\begin{lstlisting}[style=output]
 What is the size of the sample set, n(S)?

 >> 8

 How many game value outcomes are there? (How many x[i] items?)

 >> 4

x[1] = 3
How many events lead to this score? 1
Probability calculated as 0.125%.

x[2] = 2
How many events lead to this score? 3
Probability calculated as 0.375%.

x[3] = 1
How many events lead to this score? 3
Probability calculated as 0.375%.

x[4] = 0
How many events lead to this score? 1
Probability calculated as 0.125%.
\end{lstlisting}

        \paragraph{Main menu after game data entry:} ~\\

\begin{lstlisting}[style=output]
-----------------------------------------------------
 | Main Menu |
 -------------

Current game data:

Outcome #      Score value         Probability         
0              3                   0.125%              
1              2                   0.375%               
2              1                   0.375%               
3              0                   0.125%               


 1.	Enter game info
 2.	Calculate expected average
 3.	Quit   
\end{lstlisting}


        \paragraph{Computing expected average:} ~\\

\begin{lstlisting}[style=output]
E[X] = 3 * 0.125 + 2 * 0.375 + 1 * 0.375 + 0 * 0.125

Calculated expected value is: 1.5
\end{lstlisting}

    \newpage
    \subsection*{Turn in}

    Turn in your source file(s) and a screenshot of your program running.
        
\input{../BASE-4-FOOT}
