\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: Intro and Structures}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.1, 5.2, 5.3, 5.4}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 6.1, 6.2, 6.3}
\newcounter{question}

\toggletrue{answerkey}      %\togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}

    \section*{\laTitle}
    \footnotesize
    
    \begin{intro}{Types of structures}
        These are the four types of structures we will be working with
        in this section.

        \begin{center}
            \begin{tabular}{l | c | c | c }
                \textbf{}   & \textbf{Repeats}      & \textbf{Order} & \textbf{Formula}
                \\          & \textbf{allowed?}     & \textbf{matters?}     &

                \\ \hline
                \textbf{Permutation} &  &  &
                \\ $n$ items to choose from & no & yes & $P(n,r) = \frac{n!}{(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Set} &  &  &
                \\ $n$ items to choose from & no & no & $C(n,r) = \frac{n!}{r!(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Ordered list} &  &  &
                \\ $n$ items to choose from & yes &  yes & $n^{r}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list} &  &  &
                \\ $n$ different types & yes & no & $C(n+r-1, n)$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list}  &  &  &
                \\ \textbf{for Binary Strings} &  &  &
                \\ $r$ 1's & yes & no & $C(n, r)$
                \\ $n-r$ 0's & &
            \end{tabular}
        \end{center}

        The challenging part of combinatorics is \textbf{figuring out which structure}
        is described in a problem, and what values to plug in. In this part
        we will get acquainted with simple problems for each structure,
        and in future exercises we will look at rules needed for more sophisticated problems.
    \end{intro} \normalsize

    \newpage

    \subsection*{Review: Unordered Lists}

        \begin{center}
            \textbf{Order DOESN'T matter} \tab \textbf{Repeats allowed}
            \small
            
            Selection way: Given $n$ types of items and $r$ items to choose, there are $C(n+r-1, r)$ ways to choose. ~\\~\\
            Binary String way: Given $r$ 1's and $n-r$ 0's, there are $C(n,r)$ ways to choose.
            \normalsize
        \end{center}

        \small
        I updated these questions on the exercise as well, but try solving both these questions again both ways
        to learn more about unordered lists - the trickiest of the combinatorics structures.
        \normalsize
        
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}

        \begin{wrapfigure}{r}{0.2\textwidth}
            \begin{center}
                \includegraphics[height=2cm]{images/donutbox2.png}
            \end{center}
        \end{wrapfigure}

        At a donut shop, we are going to buy 6 donuts. The store sells 3 types of donuts.
        How many ways are there to buy 6 donuts?

        \begin{itemize}
            \item   Selection way:
                \begin{itemize}
                    \item   We are choosing $r$ donuts:
                            \vspace{0.5cm}
                    \item   There are $n$ options for each donut:
                            \vspace{0.5cm}
                    \item   Result, $C(n+r-1,r)$:
                            \vspace{0.5cm}
                \end{itemize}
            \item   Binary string way: ~\\ We're going to consider 0's to represent the donuts. \\
                        We're going to consider 1's to be separators \textit{between each type} of donut,
                        meaning that if there are 3 types of donuts, there are 2 \textit{separators}.
                \begin{itemize}
                    \item   There are $r$ 1's:
                            \vspace{0.5cm}
                    \item   There are $n-r$ 0's:
                            \vspace{0.5cm}
                    \item   $n$ is:
                            \vspace{0.5cm}
                    \item   Result, $C(n,r)$:
                            \vspace{1cm}
                \end{itemize}
        \end{itemize}
    \end{questionNOGRADE}
        
    \newpage

    \solution{
        \begin{itemize}
            \item   Selection way:
                \begin{itemize}
                    \item   We are choosing $r$ donuts: 6
                    \item   There are $n$ options for each donut: 3
                    \item   Result, $C(n+r-1,r)$: $C(6 + 3 - 1, 6) = C(8,6) = \frac{8!}{6! (8-6)!}$ \\
                            $= \frac{8 \cdot 7 \cdot 6 \cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1}{(6 \cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1) (2 \cdot 1)}$
                            $= \frac{8 \cdot 7}{2 \cdot 1} = 28$
                \end{itemize}
            \item   Binary string way:
                \begin{itemize}
                    \item   There are $r$ 1's: 3 types of donuts, so 2 separators.
                    \item   There are $n-r$ 0's: selecting 6 donuts.
                    \item   $n$ is: $n-2 = 6$, $n = 8$
                    \item   Result, $C(n,r)$: $C(8,2) = \frac{8!}{2! (8-2)!} = \frac{8 \cdot 7 \cdot 6 \cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1}{(6 \cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1) (2 \cdot 1)}$ \\
                            $= \frac{8 \cdot 7}{2 \cdot 1} = 28$
                \end{itemize}
        \end{itemize}
    }

    Notice that $C(8,2)$ is equivalent to $C(8,6)$.
    Now onto the actual homework!

    \newpage
    \section*{Homework}

        Make sure to identify \textbf{the structure} used in each question,
        your $n$ and $r$ values, and then solve.
    
    \begin{enumerate}
        \item   On an arcade machine, the high score table allows you to enter three letters - all capitals.
                How many ways can you enter in a name in the high score table?
        \item   How many ways are there to select two cards out of a standard deck of 52 cards?
                \deckofcards
        \item   How many ways are there to select two heart cards out of a deck?
        \item   There is a box of 20 marbles. You're going to select 5 out of the box. How many ways are there to select the marbles?
        \item   There's a line of 10 children that need to queue up for lunch. How many ways are there for them to line up?
        \item   At a bakery, there are 3 types of muffins to choose from, and you'd like to buy two for yourself.
                How many ways are there to buy your muffins?
                \inlinehint{For this question, assume you're not depleting the muffin supply; no muffins are removed from the pool once selected.}
        \item   You return to the same bakery with your buddy. You're going to buy one muffin for yourself, and one for your friend.
                How many ways are there to buy 2 muffins?
        \item   In an online RPG, you're looking to build a party of 3 members (excluding yourself). There are
                12 people in the ``Looking For Group'' area. How many ways are there to build a party?
        \item   In a class of 20 people, your'e goign to elect a President, Vice President, and Secretary. How many ways are there to elect these positions?
    \end{enumerate}


    \newpage
    \solution{
    \section*{Answer key}

    \begin{enumerate}
        \item   There are 26 letters in the alphabet, so $n = 26$. We are selecting three letters in the name, so $r = 3$.
                The structure will be an \textbf{Ordered List} because order matters (``CAT'' and ``ACT'' are different entries),
                and we can repeat letters (``AAA'', ``MOO'', ...). \\ The result is $n^{r} = 26^{3} = 17,576$
        \item   There are 52 cards to choose from, so $n = 52$. We are selecting two, so $r = 2$.
                The structure will be a \textbf{Permutation} because order matters; selecting the first card (52 options)
                will leave less options for the second card (51 options). Duplicates are not allowed because
                each card only happens once. \\
                $P(52, 2) = \frac{52!}{(52-2)!} = \frac{52 \cdot 51 \cdot 50 \cdot 49 \cdot ... \cdot 1}{50 \cdot 49 \cdot ... \cdot 1}$
                $ = 52 \cdot 51 = 2,652$
        \item   There are 13 of each suit of cards, so 13 hearts means $n = 13$. Still selecting two cards, so $r = 2$. \\
                Result: $P(13,2) = \frac{13!}{(13-2)!} = \frac{13!}{11!} = 13 \cdot 12 = 156$
        \item   $n = 20$, $r = 5$, once you select a marble it will no longer be in the box (no duplicates).
                The order doesn't matter, since we're just going to have a handful of marbles. This is a \textbf{Set}. \\
                Result: $C(20,5) = 15,504$
        \item   We have ten children, $n = 10$, and we need to line up all ten, so $r = 10$. This will be a \textbf{Permutation}
                since, once a child is in the line, they're no longer in the ``pool'' of children to choose from.
                No duplicates, since a child won't be in different positions in the line. \\
                Result: $P(10,10) = \frac{10!}{(10-10)!} = \frac{10!}{0!} = \frac{10!}{1} = 10! = 10 \cdot 9 \cdot ... \cdot 2 \cdot 1 = 3,628,800$
        \item   There are 3 types of muffins, so $n = 3$. We are going to buy two, so $r = 2$.
                Repetitions are allowed; you can buy the same muffin multiple times.
                Order doesn't matter; all the muffins are for you. \{ Chocolate, Banana Nut \} would be the same as \{ Banana Nut, Chocolate \}.
                This is a \textbf{Unordered List}. \\
                Result: $C(3+2-1, 2) = C(4,2) = 6$. \\
                We can also list these out: \\
                \{  \{ A, A \}, \{ A, B \}, \{ A, C \}, \tab \{ B, B \} \{ B, C \}, \tab \{ C, C \} \}
        \item   In this case, order does matter - buying a Chocolate for you and a Banana Nut for your friend is different from
                buying a Banana Nut for you and a Chocolate for your friend. Think of this as writing out the data in an \textbf{ordered pair}... (YOU, FRIEND)
                The structure here is an \textbf{Ordered List}. \\
                Result: $n^{r} = 3^{2} = 9$ \\
                Let's list them out... \\
                \{ (A, A), (A, B), (A, C), \tab[0.2cm] (B, A), (B, B), (B, C) \tab[0.2cm] (C, A), (C, B), (C, C) \}
        \newpage
        \item   There are 12 people to choose from, so $n = 12$. We're going to select 3, so $r = 3$.
                Order doesn't matter; everybody will be in the same party. \\ \{ Gandalf, Gimli, Frodo \} will be
                the same party as \\ \{ Frodo, Gimli, Gandalf \}. Repetitions aren't allowed, each person can
                only fill one spot in the party. This is a \textbf{Set}. \\
                Result: $C(12,3) = 220$.
        \item   There are 20 people to choose from, $n = 20$, and we're electing 3 members, $r = 3$.
                Order matters, since there are three different roles. (P, V, S).
                Duplicates are not allowed; each person can only fill one role. This is a \textbf{Permutation}. \\
                Result: $P(20,3) = 6,840$
                
    \end{enumerate}
    }{}

\input{../BASE-4-FOOT}
