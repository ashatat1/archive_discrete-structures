\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: Permutations, Rule of Sums, Rule of Products, Rule of Complements}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.2}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 6.2, 6.3}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}


    \subsection*{Permutations}

    \begin{intro}{Permutation formula}
        A permutation is a type of structure that describes counting when
        \textbf{order matters} and \textbf{repetitions are not allowed}.

        ~\\  A permutation is written as $P(n, r)$ where $n$ is the amount of
        items we have to choose from, and $r$ is the amount of items we
        are selecting. The formula for this is:

        $$ P(n, r) = \frac{n!}{(n-r)!} $$

        ~\\ Where $n!$ is $n$-factorial. Also note that $0! = 1$.
    \end{intro}
    

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Calculate the following by hand using the formula:

        \begin{enumerate}
            \item[a.]   $P(5, 2)$   \vspace{1cm}
            \item[b.]   $P(5, 5)$   \vspace{1cm}
            \item[c.]   $P(5, 1)$   \vspace{0.5cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Use \texttt{www.wolframalpha.com} or a graphing calculator to compute the following:

        \begin{enumerate}
            \item[a.]   $P(26, 3)$   \vspace{1cm}
            \item[b.]   $P(52, 6)$   \vspace{1cm}
            \item[c.]   $P(26, 2) + P(10, 2)$   \vspace{1cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \hrulefill
    
    \subsection*{The Rule of Sums}

    \begin{intro}{The Rule of Sums}
        In combinatorics, the rule of sum or addition principle is a basic counting principle.
        Stated simply,
        \textbf{it is the idea that if we have A ways of doing something and B ways of doing
        another thing and we can not do both at the same time, then there are A + B ways to choose one of the actions.}
        \footnote{From https://en.wikipedia.org/wiki/Rule\_of\_sum}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion} \small
        Uttam wants to read a new book series. He is only going to pick up two books of a series to start with.
        The serieses he's looking at are ``Native Tongue", which has \textit{3 books},
        or ``Seed to Harvest", which has \textit{4 books}.
        How many ways can Uttam select books, if he is choosing 2 Native Tongue books, OR
        2 Seed to Harvest books, but not both?
           \vspace{1cm}
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Jennifer is trying to set up their class schedule so it won't
        interfere with work. Jennifer can only take time off work for either mornings, or nights, or weekends.
        They can fit in the following amount of classes for each schedule:
        \begin{itemize}
            \item Morning classes: 3 per week, or
            \item Night classes: 3 per week, or
            \item Weekend classes: 4 per week
        \end{itemize}
        
        The school Jennifer goes to has 12 classes she can take. How many ways are there for her
        to select either 3 morning classes, or 3 night classes, or 4 weekend classes, out of the 12 available?
        
    \end{questionNOGRADE}
        
        \begin{hint}{\ }
            When questions are phrased as ``can choose \textbf{either ... OR ...}",
            this usually points to \textbf{adding} the size of both sets.
        \end{hint}

    \newpage

    \subsection*{The Rule of Complements}
    
    \begin{intro}{The rule of complements}
        If there are $x$ objects, and $y$ of those objects have a particular property,
        then the number of those objects that do \textbf{not} have that particular
        property is $x - y$.
        \footnote{From Discrete Mathematics, Ensley and Crawley, page 390}
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Assume you are rolling two dice.

        \begin{enumerate}
            \item[a.]   List out all possible results, assuming order does matter.
            \begin{center}
                \Large 
                \begin{tabular}{ c | p{1cm} p{1cm} p{1cm} p{1cm} p{1cm} p{1cm} }
                    & 1 & 2 & 3 & 4 & 5 & 6
                    \\ \hline
                    1 \\ 2 \\ 3 \\ 4 \\ 5 \\ 6 
                \end{tabular}
            \end{center}

            \item[b.]   How many total outcomes are there?      \vspace{1cm}

            \item[c.]   How many results have 6 show up at least once?  \vspace{1cm}

            \item[d.]   Use the rule of complements to solve how many
                        outcomes there are where 6 does not show up.    \vspace{1cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    
    \begin{intro}{The rule of sums with overlap}
        If the list to count can be split into two pieces of size $x$
        and $y$, and the pieces have $z$ objects in common, then the original
        list has $x + y - z$ entries. In terms of sets, we can write this as
        $n(A \cup B) = n(A) + n(B) - n(A \cap B)$ for all sets $A$ and $B$.
    \footnote{From Discrete Math Mathematical Reasoning and Proofs with Puzzles, Patterns and Games, by Ensley and Crawley}
    \end{intro}


    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
            Ryan has 107 games in his Steam library.
            Of those, 32 have the category ``action",
            14 have the category ``RPG",
            and 8 are categorized as both ``action" AND ``RPG", so they end up getting double-counted.

        \begin{enumerate}
            \item[a.] How many games are action or RPG? \\(Use the rule of sums with overlap)   \vspace{2cm}
            \item[b.] How many games are NOT action or RPG? \\(Use the rule of complements)     \vspace{2cm}
        \end{enumerate}        
    \end{questionNOGRADE}

    \newpage

    \subsection*{The Rule of Products}

    \begin{intro}{The rule of products}
        In combinatorics, the rule of product or multiplication principle is a
        basic counting principle (a.k.a. the fundamental principle of counting).
        Stated simply,
        \textbf{it is the idea that if there are $a$ ways of doing something and $b$
        ways of doing another thing, then there are $a \cdot b$ ways of performing both actions.}
        \footnote{From https://en.wikipedia.org/wiki/Rule\_of\_product}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        On an old arcade machine, the highscore entries allow for
        3 letters for a player to sign their name. Many only offer capital letters,
        so only 26 options, and each letter can be used more than once.
        
        \begin{enumerate}
            \item[a.]   How many options are there for the first letter?        \vspace{1cm}
            \item[b.]   How many options are there for the second letter?       \vspace{1cm}
            \item[c.]   How many options are there for the third letter?        \vspace{1cm}
            \item[d.]   If you're choosing a first letter \textbf{and} a second letter \textbf{and} a third letter,
                        how many possible combinations are there?               \vspace{1cm}
        \end{enumerate}
    \end{questionNOGRADE}
    


\input{../BASE-4-FOOT}
