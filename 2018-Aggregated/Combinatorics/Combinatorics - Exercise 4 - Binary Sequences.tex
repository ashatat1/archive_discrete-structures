\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: Binary Sequences}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.4}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 6.3}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}
    
    \subsection*{Binary sequences}

    \begin{intro}{Binary sequence theorem}
        The number of binary sequences with $r$ 1's and $n-r$ 0's is $C(n,r)$ or $C(n, n-r)$.
        \footnote{From Discrete Mathematics, Ensley and Crawley, page 409}

        ~\\
        This can be used for straightforward problems, like ``how many binary sequences are there
        with $x$ 1's and $y$ 0's?'', but we can also use it for more practical applications...!
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        How many binary sequences are there with three 1's and two 0's?
        
        \begin{enumerate}
            \item[a.]   How many 1's? $r = $
            \item[b.]   How many 0's? $n-r = $
            \item[c.]   What is the value of $n$?
            \item[d.]   How many binary sequences are there? $C(n,r) = $
        \end{enumerate}
    \end{questionNOGRADE}
    
    \hrulefill
       
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Assuming you're building a binary string of length 5,
        using the Binary Sequence theorem and the Sum Rule, 
        find the number of binary sequences that have an odd number of 1's.
        \\ \textit{(Hint: this means one 1, three 1's, or five 1's; you can figure out the 0's from that.)}
    \end{questionNOGRADE}
    
    \newpage
    
    \begin{intro}{Other uses of Binary Sequences}
        We can also use this theorem for strings that have more than just 0's and 1's...
        
        \paragraph{Example 2 from the book:} How many ordered lists of 10 letters, chosen from $\{m, a, t\}$, 
        have exactly three $m$'s?
        
        If we think in terms of spots to fill for each letter, we can diagram it like this:
        
        \begin{center}
            \begin{tabular}{c c c c c c c c c c}
                \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm] & \fitb[0.5cm]
                \\
                1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10
            \end{tabular}
        \end{center}
        
        ~\\ Since we have a restriction on the $m$'s, these should be filled in first. Selecting three $m$'s gives us
        $C(10,3) = 120$ combinations.
        
        ~\\ Then, since there are no restrictions on the rest of the letters, we can select the final 7.
        However, we don't have 3 to choose from anymore; we wanted \textbf{exactly} 3 $m$'s and we have
        already filled those. So, instead of selecting from 3 options, we only have two: $\{a, t\}$.
        
        ~\\ Since for each of the remaining, we have two options each, it will be calculated as $2 \cdot 2 \cdot 2 \cdot ...$
        seven times. These remaining 2's are an \textbf{ordered list}: Order matters, and we can have duplicates ($a$ or $t$).
        
        ~\\ Our final result will be $C(10,3) \cdot 2^{7}$.
    \end{intro}
    
    \begin{intro}{Formulas for each structure type}
    
        \begin{center}
            \begin{tabular}{l | c | c | c }
                \textbf{}
                    & \textbf{Repeats}
                    & \textbf{Order}
                    & \textbf{}
                \\
                \textbf{Type}
                    & \textbf{allowed?}
                    & \textbf{matters?}
                    & \textbf{Formula}
                \\ \hline
                Ordered list of length $r$
                    & yes
                    & yes
                    & $n^{r}$

                \\ \hline
                Unordered list of length $r$
                    & yes
                    & no
                    & $C(r + n - 1, r)$
                \\ \hline
                Permutations of length $r$
                    & no
                    & yes
                    & $P(n,r) = \frac{n!}{(n-r)!}$
                \\ \hline
                Sets of length $r$
                    & no
                    & no
                    & $C(n,r) = \frac{n!}{r!(n-r)!}$
            \end{tabular}
        \end{center}
        
    \end{intro}

    We can also use what we've learned on application problems, where we need to
    group up items together.

    \newpage

    \begin{intro}{Modeling problems with binary sequences}
        Let's say you have a box that can store 8 donuts.
        If you're going to put multiple flavors in the box, you will
        use a separator to keep them apart.

        \begin{center}
            \begin{tabular}{c c}
                Same donuts & 
                Different donuts
                \\ \\
                \centering
                \includegraphics[width=6cm]{images/donutbox1.png}
                &
                \includegraphics[width=6cm]{images/donutbox2.png}
            \end{tabular}
        \end{center}

        In this case, we don't care what kind of donuts are in the box;
        we will represent them with zeroes. The splitters, we will represent as 1.
        With this information, we will model our box of donuts as a binary string.

        \begin{center}
            \includegraphics[height=4cm]{images/donutbox3.png}
        \end{center}
    \end{intro}

    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following donut combinations, draw the donut box, the donuts, and the separators.
        Also list out the binary sequence that represents the box.
        Assume that the box must always have 2 separators. If a separator isn't being used, it can
        go at either end of the box.

        \begin{enumerate}
            \item[a.]   One Chocolate, Three Strawberry, and Four Glazed    \vspace{5cm}
            \item[b.]   Four Chocolate and Four Glazed  \vspace{5cm}
            \item[c.]   Eight Glazed    \vspace{3cm}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \begin{intro}{Theorem}
        Let natural numbers $n$ and $r$ be given.
        \footnote{Theorem 3 from Discrete Mathematics, Ensley and Crawley, page 413}

        \begin{enumerate}
            \item   The number of solutions to the equation $x_{1} + ... + x_{n} = r$
            using nonnegative integers is $C(r + n - 1, r)$.
            \item   The number of unordered lists of length $r$ taken from a set of size $n$,
            which repetitions allowed, is $C(r + n - 1, r)$.
            \item   The number of bags of $r$ pieces of fruit that can be bought at a store with $n$
            types of fruit available is $C(r + n - 1, r)$.
        \end{enumerate}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        At Darrell's Donuts, Darrell sells 5 different types of donuts.
        He only sells 12-donut boxes. How many different combinations
        of donut boxes can you build?
        \footnote{Based on Example 6 from Discrete Mathematics, Ensley and Crawley, page 413}

        \begin{enumerate}
            \item[a.]   How many types of donuts are there? $n =$
            \item[b.]   How many donuts will be selected? $r = $
            \item[c.]   How many separators should there be?
                        \textit{(Hint: It should be one less than the donut types)}
                        \vspace{0.5cm}
            \item[d.]   If donuts and separators both count as spaces in the box,
                        how many available spaces are there total?
                        \vspace{0.5cm}
            \item[e.]   What is $r + n - 1$?
                        \vspace{0.5cm}
            \item[f.]   What is the Combination formula for finding out the total ways you can build your donut boxes?
                        \vspace{0.5cm}
            \item[g.]   What is the final answer?
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        At Piper's Produce, there are 3 types of fruit for sale.
        How many ways can you fill a bag that can store 20 pieces of fruit?
    \end{questionNOGRADE}


\input{../BASE-4-FOOT}
