#hextodecimal.py

def hexToDecimal(h):  # doing the work myself
    hexValue = 0
    lengthOfHex = len(h)
    for digit in h:
        lengthOfHex -= 1
        if digit.upper() == 'A':
            digit = 10
        if digit.upper() == 'B':
            digit = 11
        if digit.upper() == 'C':
            digit = 12
        if digit.upper() == 'D':
            digit = 13
        if digit.upper() == 'E':
            digit = 14
        if digit.upper() == 'F':
            digit = 15
        hexValue += int(digit) * (16 ** lengthOfHex)
    
    return hexValue

def hexToDecimal2(h):  # using the int function
    return int(h, 16)
    
def main():
    h = input("Enter a hex number of at least 0: ")
    print(hexToDecimal(h))
    print(hexToDecimal2(h))

main()
        
