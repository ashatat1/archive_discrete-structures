# sum1SquaredToNSquared

def sum1SquaredToNSquared(n):
    return int(n * (n + 1) * (2 * n + 1) / 6)
    
def main():
    n = int(input("Enter a positive integer: "))
    print("Sum =" , sum1SquaredToNSquared(n))

main()
