# sum1ToNAndCubedTable


def sum1ToN(n):
    return int(n * (n + 1) / 2)

def sum1CubedToNCubed(n):
    return int((n * (n + 1) / 2)**2)

def main():
    n = int(input("Enter a positive integer: "))
    print(n , ":" , "\t\t" , sum1ToN(n) , "\t\t" , sum1CubedToNCubed(n))

main()
