\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {HW3A Boolean Algebra - Intro to Boolean Algebra}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 3.4}
\newcommand{\laTextbookB}   {}
\newcommand{\laTextbookC}   {ZyBooks: Chapter 3.1}
\newcounter{question}
\renewcommand{\chaptername}{Part}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}

\notonkey{

\chapter{Overview - Intro to Boolean Algebra}


    %---%
    \section{Intro to Boolean Algebra}

    Remember that when we were working with \textbf{propositional logic},
    we were concerned with \textbf{true} and \textbf{false} outcomes given
    some propositional variables ($p$, $q$, etc.) or predicates ($P(x)$, $Q(x)$, etc.).
    With \textbf{Boolean Algebra}, we are abstracting out logic into
    a way we can represent with algebraic notation.
    ~\\

    We continue using variables in boolean algebra, such as $p$ and $q$,
    and we represent ``true'' with the number 1,
    and ``false'' with the number 0. Our $\land$ (and) logical operator
    is represented as $\cdot$ or multiplication between the variables,
    and $\lor$ (or) becomes represented with $+$. And a negation $\neg$
    becomes represented as a prime-marker ($\neg p$ becomes $p'$).
    ~\\

    You may also notice a relationship between logic and boolean algebra as
    well as with sets. We are using similar patterns with each type of these problems,
    even though each type may be used for different types of problems.

    \begin{center}
        \begin{tabular}{l | c c c}
            \textbf{Symbol} & \textbf{Logic}    & \textbf{Boolean algebra}  & \textbf{Sets} \\ \hline
            Variables       & $p$, $q$, $r$     & $a$, $b$, $c$             & $A$, $B$, $C$ \\ \\
            AND operator    & $\land$           & $\cdot$                   & $\cap$ \\
            OR operator     & $\lor$            & $+$                       & $\cup$ \\
            NOT operator    & $\neg$            & $'$                       & $'$ \\ \\
            DIFFERENCE      & $a \land \neg b$  & $a \cdot b'$              & $A - B$
        \end{tabular}
    \end{center}

    \newpage
    ~\\
    In \textbf{logic}, we could have logical statements that are \textbf{contradictions} - where,
    no matter what the state of the variables, the outcome was always false -
    or statements that are \textbf{tautologies} - no matter what the state of the variables,
    the result is always true.

    ~\\
    With \textbf{sets}, we could have a set operation that results in $U$, \textbf{the universal set},
    or an operation that results in $\emptyset$, \textbf{an empty set}.

    ~\\
    And in \textbf{boolean algebra}, our statements could result in 1 or 0.

    %---%
    \section{Boolean algebra properties}

    As with logic and sets, we also have commutative properties, associative properties, and so on.
    
    ~\\
    Table of boolean algebra properties \footnote{Adapted from Discrete Mathematics, Ensley \& Crawley, pg 225}
    ~\\~\\
    \begin{tabular}{ l l p{4cm} p{5cm} }
           (a) & Commutative       & $a \cdot b = b \cdot a$                            & $a + b = b + a$            
        \\ (b) & Associative       & $(a \cdot b) \cdot c) = a \cdot (b \cdot c)$       & $(a + b) + c = a + (b + c)$ 
        \\ (c) & Distributive      & $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$      & $a + (b \cdot c) = (a + b) \cdot (a + c)$
        \\ (d) & Identity          & $a \cdot 1 = a$                                    & $a + 0 = a$
        \\ (e) & Negation          & $a + a' = 1$                                       & $a \cdot a' = 0$
        \\ (f) & Double negative   & $(a')' = a$
        \\ (g) & Idempotent        & $a \cdot a = a$                                    & $a + a = a$
        \\ (h) & DeMorgan's laws   & $(a \cdot b)' = a' + b'$                           & $(a + b)' = a' \cdot b'$
        \\ (i) & Universal bound   & $a + 1 = 1$                                        & $a \cdot 0 = 0$
        \\ (j) & Absorption        & $a \cdot (a + b) = a$                              & $a + (a \cdot b) = a$
        \\ (k) & Complements of    & $1' = 0$                                           & $0' = 1$
        \\     & 1 and 0

    \end{tabular}


    %---%
    \section{Operations in boolean algebra}

    Simplifying equations in boolean algebra isn't going to be 1-to-1 the same as
    normal algebra; for example, if you have $a + a$, the result isn't going to be $2a$ here.
    Thinking in terms of logic, $a + a$ would be ``$a$ or $a$'', and logically, that would simplify
    to just $a$. You can also refer to the \textbf{Idempotent Property}, which states
    that $a + a = a$ and $a \cdot a = a$.

    %---%
    \section{Properties in logic, sets, and boolean algebra}
    
    ~\\
    Tables of logic, set, and boolean algebra properties \footnote{Adapted from Discrete Mathematics, Ensley \& Crawley, pg 222, 225}
    ~\\~\\
    \begin{tabular}{ p{2cm} | p{5cm} | p{5cm} }
                                    & \textbf{Commutative Property} & \textbf{Associative Property}
        \\ \hline
        \textbf{Logic}              & $p \land q \equiv q \land p$  & $(p \land q) \land r \equiv p \land (q \land r)$
        \\                          & $p \lor q \equiv q \lor p$    & $(p \lor q) \lor r \equiv p \lor (q \lor r)$
        \\ \hline
        \textbf{Sets}               & $A \cap B = B \cap A$         & $(A \cap B) \cap C = A \cap (B \cap C)$
        \\                          & $A \cup B = B \cup A$         & $(A \cup B) \cup C = A \cup (B \cup C)$
        \\ \hline
        \textbf{Boolean}            & $a \cdot b = b \cdot a$       & $(a \cdot b) \cdot c = a \cdot (b \cdot c)$
        \\ \textbf{Algebra}         & $a + b = b + a$               & $(a + b) + c = a + (b + c)$
    \end{tabular}
    ~\\~\\~\\~\\
    \begin{tabular}{ p{2cm} | p{5cm} | p{5cm} }
                                    & \textbf{Distributive Property}                                & \textbf{Double negative}
        \\ \hline
        \textbf{Logic}              & $p \land (q \lor r) \equiv (p \land q) \lor (p \land r)$      & $\neg(\neg p) \equiv p$
        \\                          & $p \lor (q \land r) \equiv (p \lor q) \land (p \lor r)$
        \\ \hline
        \textbf{Sets}               & $A \cap (B \cup C)$                                           & $(A')' = A$
        \\                          & \tab $= (A \cap B) \cup (A \cap C)$
        \\                          & $A \cup (B \cap C)$
        \\                          & \tab $= (A \cup B) \cap (A \cup C)$
        \\ \hline
        \textbf{Boolean}            & $a \cdot (b + c) = (a \cdot b) + (a \cdot c)$                 & $(a')' = a$
        \\ \textbf{Algebra}         & $a + (b \cdot c) = (a + b) \cdot (a + c)$
    \end{tabular}

    \newpage

    \begin{tabular}{ p{2cm} | p{3cm} | p{3cm} | p{3cm} }
                                    & \textbf{Identity}             & \textbf{Negation}         & \textbf{Idempotent}
        \\ \hline
        \textbf{Logic}              & $p \land t \equiv p$          & $p \lor \neg p \equiv t$  & $p \land p \equiv p$
        \\                          & $p \lor c \equiv p$           & $p \land \neg p \equiv c$ & $p \lor p \equiv p$
        \\ \hline
        \textbf{Sets}               & $A \cap U = A$                & $A \cup A' = U$           & $A \cap A = A$
        \\                          & $A \cup \emptyset = A$        & $A \cap A' = \emptyset$   & $A \cup A = A$
        \\ \hline
        \textbf{Boolean}            & $a \cdot 1 = a$               & $a + a' = 1$              & $a \cdot a = a$
        \\ \textbf{Algebra}         & $a + 0 = a$                   & $a \cdot a' = 0$          & $a + a = a$
    \end{tabular}
    ~\\~\\~\\~\\
    \begin{tabular}{ p{2cm} | p{4cm} | p{3cm} | p{3cm} }
                                    & \textbf{DeMorgan's Laws}                              & \textbf{Universal bound}          & \textbf{Absorption} 
    \\ \hline
    \textbf{Logic}                  & $\neg (p \land q) \equiv \neg p \lor \neg q$          & $p \lor t \equiv t$               & $p \land (p \lor q) \equiv p$
    \\                              & $\neg (p \lor q) \equiv \neg p \land \neg q$          & $p \land c \equiv c$              & $p \lor (p \land q) \equiv p$
    \\ \hline
    \textbf{Sets}                   & $(A \cap B)' = A' \cup B'$                            & $A \cup U = U$                    & $A \cap (A \cup B) = A$
    \\                              & $(A \cup B)' = A' \cap B'$                            & $A \cap \emptyset = \emptyset$    & $A \cup (A \cap B) = A$
    \\ \hline
    \textbf{Boolean}                & $(a \cdot b)' = a' + b'$                              & $a + 1 = 1$                       & $a \cdot (a + b) = a$
    \\ \textbf{Algebra}             & $(a + b)' = a' \cdot b'$                              & $a \cdot 0 = 0$                   & $a + (a \cdot b) = a$
    \end{tabular}
    ~\\~\\~\\
    \begin{tabular}{ p{2cm} | l }
                                    & \textbf{Negations of $t$ and $c$} \\
    \\ \hline
    \textbf{Logic}                  & $\neg t \equiv c$
    \\                              & $\neg c \equiv t$
    \\ \\
                                    & \textbf{Complements of $U$ and $\emptyset$}
    \\ \hline
    \textbf{Sets}                   & $U' = \emptyset$ 
    \\                              & $\emptyset' = U$
    
    \\ \\
                                    & \textbf{Complements of 1 and 0}
    \\ \hline
    \textbf{Boolean}                & $1' = 0$
    \\ \textbf{Algebra}             & $0' = 1$

    \end{tabular}
    


}{}

\chapter{Questions}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}
        
    \stepcounter{question} \question{\thequestion}{Logic to Boolean Algebra}{30\%}
    
    Rewrite the following \textbf{logic} expression to be a \textbf{boolean algebra} expression.
    
    \begin{itemize}
        \item[example:]  $p \land q$    \tab[2cm]    \color{red} $p \cdot q$ \color{black}
        \item[example:]  $p \lor q$    \tab[2cm]    \color{red} $p + q$ \color{black}
        
        \item[a.]   $\neg p$ 
                    \solution{ $p'$ }{ \vspace{1cm} }
        \item[b.]   $(p \land \neg q) \lor p$ 
                    \solution{$((p \cdot q') + p$}{ \vspace{1cm} }
        \item[c.]   $\neg( \neg p ) )$ 
                    \solution{ $(p')' = p$ }{ \vspace{1cm} }
    \end{itemize}
    
    \newpage
    
    \stepcounter{question} \question{\thequestion}{Sets to Boolean Algebra}{30\%}
    
    Rewrite the following \textbf{set} expression to be a \textbf{boolean algebra} expression.
    
    \begin{itemize}
        \item[example:]  $A \cap B$    \tab[2cm]    \color{red} $a \cdot b$ \color{black}
        \item[example:]  $A \cup B$    \tab[2cm]    \color{red} $a + b$ \color{black}
        
        \item[a.]   $(A - B)$ 
                    \solution{ $a \cdot b'$ }{ \vspace{3cm} }
        \item[b.]   $A' \cup (A \cap B)$ 
                    \solution{ $ a' + (a \cdot b) $ }{ \vspace{3cm} }
        \item[c.]   $(A - B)' = A' \cup (A \cap B)$ 
                    \solution{ $ (a \cdot b')' = a' $ }{ \vspace{3cm} }
    \end{itemize}
    
    \newpage
    \stepcounter{question} \question{\thequestion}{Simplifying expressions}{40\%}
    
    Simplify the following expressions using the Boolean Algebra properties.
    You can verify using a truth table.
    
    \begin{itemize}
        \item[example:]  $x \cdot (x' + y)$    \\
                        \color{red}
                        Distributive property:  $= (x \cdot x') + (x \cdot y)$ \\
                        Negation: $= (0) + (x \cdot y)$ \\
                        Identity: $= x \cdot y$ \\
                        
                        Check:
                        
                        \begin{tabular}{c c | c | c }
                            $x$ & $y$   & $x \cdot (x' + y)$    & $x \cdot y$ \\ \hline
                            1   & 1     & 1                     & 1
                            \\
                            1   & 0     & 0                     & 0
                            \\ 
                            0   & 1     & 0                     & 0
                            \\
                            0   & 0     & 0                     & 0
                        \end{tabular}
                        \color{black}
        
        \item[a.]   $ xy + xy' $
                    \solution{  
                    \\ Distributive: $=x( y + y' )$
                    \\ Identity: $= x(1) $
                    \\ $= x$
                    }{ \vspace{4cm} }
        
        \item[b.]   $ (a \cdot 1) \cdot (b + a'c) $
                    \solution{  
                    \\ Identity: $= (a) \cdot (b + a'c)$
                    \\ Distributive: $= ab + a(a')(c) $
                    \\ Negation: $= ab + (0)(c) $
                    \\ Universal bound: $= ab + 0$
                    \\ Identity: $= ab$
                    }{ \vspace{2cm} }
    \end{itemize}
    
   
   
   
   
\input{../BASE-4-FOOT}
