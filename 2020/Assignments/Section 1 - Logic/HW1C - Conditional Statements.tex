\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {HW1C Logic - Conditional Statements (Implications)}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 1.5}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 1.3}
\newcommand{\laTextbookC}   {ZyBooks: Chapter 1.5}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}

    
    \notonkey{
    \subsection*{Implications}

        \begin{intro}{Implications} ~\\
            A statement like ``if $p$ is true, then $q$ is true" is known
            as an implication. It can be written symbolically as $p \to q$,
            and read as ``p implies q" or ``if p, then q".
            For the implication $p \to q$, $p$ is the \textbf{hypothesis}
            and $q$ is the \textbf{conclusion}.

            \paragraph{Example:} Write the following statement symbolically,
                defining the propositional variable:

                \begin{center}
                    ``If it is Kate's birthday, then Kate will get a cake."
                \end{center}
                ~\\
                I will define two propositional variables:

                $b$ is ``It is Kate's birthday" \tab $c$ is ``Kate will get a cake."

                ~\\
                And then I can write it symbolically: $b \to c$.
        \end{intro}
        
    }{}


    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following statements, create two
        \textbf{propositional variables}, assign them values (from
        the original statement), and then write it symbolically.

        \begin{enumerate}
            \item[a.] IF you don't play, THEN you can't win!
                        ~\\ Variables: 
                        \solution{ $p$: you play, $w$: you win \\ }{ \tab[3cm] }
                        Symbolic statement:
                        \solution{ $\neg p \to \neg w$ }{}
            \item[b.] IF your friends don't dance, THEN they aren't friends of mine!
                        ~\\ Variables:
                        \solution{$d$: Your friends dance, $f$: They are my friends \\ }{ \tab[3cm] }
                        Symbolic statement:
                        \solution{ $\neg f \to \neg f$ }{}
            \item[c.] IF Timmy's age is over 8 AND Timmy's age is less than 13, THEN Timmy gets Tween-priced movie tickets.
                        ~\\ Variables:
                        \solution{ $e$: age is over 8, $t$: less than 13, $m$: tween-price \\ }{ \tab[3cm]  }
                        Symbolic statement:
                        \solution{ $(e \land t) \to m$ }{}
            \item[d.] IF I get enough sleep, OR I drink coffee, THEN I can go to work.
                        ~\\ Variables:
                        \solution{ $s$: get enough sleep, $c$: I drink coffee, $w$: I can go to work}{ \tab[3cm]  }
                        Symbolic statement:
                        \solution{ $(s \lor c) \to w$ }{}
        \end{enumerate}        
    \end{questionNOGRADE}
    
    \notonkey{
    
        \begin{intro}{Truth for implications}
            With an implication, ``if \textbf{hypothesis}, then \textbf{conclusion}",
            the truth table will look a bit different than what you've done so far.
            Make sure to take notice because it is easy to make an error when it
            comes to implication truth tables!

            For an implication to be logically FALSE, it must be the case that
            the \textbf{hypothesis is true, but the conclusion is false}. In
            all other cases, the implication results to \textbf{true}.

            \begin{center}
                \begin{tabular}{| c | c | c | c |}
                    \hline{}
                    $p$ & $q$ & & $p \to q$
                    \\ \hline
                    T & T & & T
                    \\ \hline

                    T & F & & F
                    \\ \hline

                    F & T & & T
                    \\ \hline

                    F & F & & T
                    \\ \hline
                \end{tabular}
            \end{center}

            Seems weird? Think of it this way: The only FALSE result is if
            the \textbf{hypothesis} is true, but the \textbf{conclusion}
            ends up being false. In science, it would mean our hypothesis
            has been disproven. If, however, the \textbf{hypothesis}
            is false, we can't really say anything about the conclusion
            (again thinking about science experiments) - it is \textbf{true by default.}
        \end{intro}
        
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $\neg( p \to q )$
        ~\\
        \large
        \begin{tabular}{c c | c | c p{6cm} }
            $p$ & $q$ & $p \to q$       & $\neg( p \to q )$
            \\ \hline
            T   & T   & \solution{T}{}  & \solution{F}{}
            \\
            T   & F   & \solution{F}{}  & \solution{T}{}
            \\
            F   & T   & \solution{T}{}  & \solution{F}{}
            \\
            F   & F   & \solution{T}{}  & \solution{F}{}
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}
    
    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $(p \land q) \to r$
        ~\\
        \Large
        \begin{tabular}{c c c | c | c p{5cm} }
            $p$ & $q$ & $r$ & $p \land q$   & $(p \land q) \to r$
            \\ \hline
            T   & T   & T   & T             & \solution{T}{} \\
            T   & T   & F   & T             & \solution{F}{} \\
            T   & F   & T   & F             & \solution{T}{} \\
            T   & F   & F   & F             & \solution{T}{} \\
            F   & T   & T   & F             & \solution{T}{} \\
            F   & T   & F   & F             & \solution{T}{} \\
            F   & F   & T   & F             & \solution{T}{} \\
            F   & F   & F   & F             & \solution{T}{} \\
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}
    \vspace{1cm}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the truth tables for the expression $p \to (q \lor r)$
        ~\\
        \Large
        \begin{tabular}{c c c | c | c p{5cm} }
            $p$ & $q$ & $r$ & $q \lor r$ &  $p \to (q \lor r)$
            \\ \hline
            T   & T   & T   & T             & \solution{T}{} \\
            T   & T   & F   & T             & \solution{T}{} \\
            T   & F   & T   & T             & \solution{T}{} \\
            T   & F   & F   & F             & \solution{F}{} \\
            F   & T   & T   & T             & \solution{T}{} \\
            F   & T   & F   & T             & \solution{T}{} \\
            F   & F   & T   & T             & \solution{T}{} \\
            F   & F   & F   & F             & \solution{T}{} \\
        \end{tabular}
        \normalsize
    \end{questionNOGRADE}
    
    \newpage
    
    \notonkey
    {
    \subsection*{Negations of Implications}

    \begin{intro}{\ }
        The negation of the implication $p \to q$ is the statement
        $p \land (\neg q)$. We can see that these are equivalent
        with a truth table:

        \begin{center}
            \begin{tabular}{c c | c | c | c }
                $p$ & $q$ & $p \to q$ & $\neg( p \to q)$ & $p \land \neg q$
                \\ \hline
                T & T & T & F & F
                \\
                T & F & F & T & T
                \\
                F & T & T & F & F
                \\
                F & F & T & F & F
            \end{tabular}
        \end{center}

        It is important to remember that \textbf{the negation of an
        implication is \underline{not} also an implication!}

        \paragraph{Example:} We're going to do science. My hypothesis is
        that ``a watched pot never boils", so let's turn this into an
        implication.

        \begin{center}
            Hypothesis $p$: ``You watch a pot." \\
            Conclusion $q$: ``The pot never boils." \\
            $p \to q$: If you watch a pot, then the pot never boils.
        \end{center}

        There are four different things that can happen:

        \begin{enumerate}            
            \item If we watch the pot ($p$), and it never boils ($q$), then our implication is true:
                We've proven it. Our implication is \textbf{true}.
            \item If we watch the pot ($p$), and it DOES boil ($\neg q$), then our hypothesis has been disproven:
                we did the experiment, and our conclusion was wrong, so the implication is \textbf{false}.
            \item If we don't watch the pot ($\neg p$), and it never boils ($q$), while our conclusion
                may be true, we didn't even bother to follow through with our hypothesis; we failed at science.
                We can't prove or disprove our original hypothesis. In this case, the implication is
                still \textbf{true}, because we haven't disproven it.
            \item If we don't watch the pot ($\neg p$), and it DOES boil ($\neg q$), same as above,
                we aren't performing our hypothesis, so this doesn't prove or disprove anything;
                the implication is still \textbf{true}.
        \end{enumerate}

        If the experiment FAILS (results in false), then it means $p \land \neg q$.
    \end{intro}

    \newpage
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For each of the following statements, write out the negation
        both symbolically and in English.

        \begin{enumerate}
            \item[a.] $i$: If you go to New Delhi, $j$: You will see the Connaught Place. \\
                $i \to c$: If you go to New Delhi, then you will see the Connaught Place. \\
                \solution{ 
                $\neg( i \to c ) \\ \equiv i \land \neg c$ \\
                ``You go to New Delhi and you don't see Connaught Place.''
                }{ \vspace{2cm} }
                
            \item[b.]
                $j$: Jessica gets chocolate,
                $c$: Jessica gets cake, \\
                $b$: Jessica has a happy birthday. \\
                $(j \lor c) \to b$: If Jessica gets chocolate or Jessica gets cake, then Jessica has a happy birthday.
                \solution{ \\
                $\neg ( (j \lor c) \to b )$ \\
                $\equiv (j \lor c) \land \neg b$\\
                $\equiv (j \lor c) \land \neg b $ \\
                ``Jessica gets chocolate or Jessica gets cake and Jessica does not have a happy birthday.''
                }{ \vspace{2cm} }
                                
            \item[c.]
                $p$: You have a group project, $w$: You do all the work, \\
                $s$: Someone else does all the work. \\
                $p \to (w \lor s)$: If you have a group project, then either you do all the work,
                    or someone else does all the work.
                \solution{ \\
                $\neg ( p \to (w \lor s) )$ \\
                $\equiv p \land \neg( w \lor s )$ \\
                $\equiv p \land \neg w \land \neg s$ \\
                ``You have a group project, and you don't do all the work, and somebody else doesn't do all the work.'' \\
                (Oh no, the work doesn't get done! I blame your classmates on being so lazy.)
                }{ \vspace{2cm} }
        \end{enumerate}        
    \end{questionNOGRADE}


% ASSIGNMENT ------------------------------------ %
   
   \notonkey{
    \newpage
    \section*{Overview questions}
    
    }{}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		For each of these implications, define variables for each proposition and rewrite the statement symbolically.
		\begin{itemize}
			\item[a.]   ``If it glitters, then it is gold.''
                        \solution{ \\
                        $g$: It glitters, $a$: It is gold. \\
                        $g \to a$
                        }{ \vspace{1cm} }
			\item[b.]   ``If you study linguistics and you study computer science, then you can get a job in natural language processing.''
                        \solution{ \\
                        $l$: You study linguistics, $c$: You study computer science, $n$: You can get a job in NLP. \\
                        $(l \land c) \to n$
                        }{ \vspace{1cm} }
			\item[c.]   ``If don't eat meat and you eat cheese, then you are a vegetarian.''
                        \solution{ \\
                        $m$: You eat meat, $c$: You eat cheese, $v$: You are vegetarian. \\
                        $(\neg m \land c) \to v$
                        }{ \vspace{1cm} }
			\item[d.]   ``If don't eat meat and you don't eat cheese, then you are vegan.''
                        \solution{ \\
                        $m$: You eat meat, $c$: You eat cheese, $g$: You are vegan. \\  
                        $(\neg m \land \neg c ) \to g)$
                        }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    
    \stepcounter{question}
	\begin{questionNOGRADE}{\thequestion}
		For the following statement, write out the negation, converse, inverse, and contrapositive in English. ~\\
		$p$ = The animal has pointy ears, $q$ = The animal is a cat, ~\\ $p \to q$ = If the animal has pointy ears, then the animal is a cat.
		\begin{itemize}
			\item[a.]   Negation: $p \land \neg q$
                        \solution{ \\
                            ``The animal has pointy ears and it is not a cat.''
                        }{ \vspace{1cm} }
			\item[b.]   Converse: $q \to p$
                        \solution{ \\
                            ``If the animal is a cat, then it has pointy ears.''
                        }{ \vspace{1cm} }
			\item[c.]   Inverse: $\neg p \to \neg q$
                        \solution{ \\
                            ``If the animal does not have pointy ears, then the animal is not a cat.''
                        }{ \vspace{1cm} }
			\item[d.]   Contrapositive: $\neg q \to \neg p$
                        \solution{ \\
                            ``If the animal is not a cat, then the animal does not have pointy ears.''
                        }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}
    
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Complete truth tables for the following expressions, then explain the state of the propositional variables that cause the false case.
		\begin{itemize}
			\item[a.]   $p \to q$
                        \solution{ \\ 
                        \begin{tabular}{c | c || c}
                            $p$ & $q$   & $p \to q$ \\ \hline
                            T   & T     & T
                            \\
                            T   & F     & F
                            \\
                            F   & T     & T
                            \\
                            F   & F     & T
                        \end{tabular}
                        }{ \vspace{3cm} }
                        
			\item[b.]   $p \to (q \lor r)$
                        \solution{ \\ 
                        \begin{tabular}{c | c | c || c || c}
                            $p$ & $q$ & $r$     & $q \lor r$    & $p \to (q \lor r)$ \\ \hline
                            T   & T   & T       & T             & T
                            \\
                            T   & T   & F       & T             & T
                            \\
                            T   & F   & T       & T             & T
                            \\
                            T   & F   & F       & F             & F
                            \\
                            F   & T   & T       & T             & T
                            \\
                            F   & T   & F       & T             & T
                            \\
                            F   & F   & T       & T             & T
                            \\
                            F   & F   & F       & F             & T
                            \\
                        \end{tabular}
                        }{ \vspace{3cm} }
                        
			\item[c.]   $p \to \neg (q \land r)$
                        \solution{ \\ 
                        \begin{tabular}{c | c | c || c || c || c}
                            $p$ & $q$ & $r$     & $q \land r$   & $\neg ( q \land r )$  & $p \to \neg (q \land r)$ \\ \hline
                            T   & T   & T       & T             & F                     & F
                            \\
                            T   & T   & F       & F             & T                     & T
                            \\
                            T   & F   & T       & F             & T                     & T
                            \\
                            T   & F   & F       & F             & T                     & T
                            \\
                            F   & T   & T       & T             & F                     & T
                            \\ 
                            F   & T   & F       & F             & T                     & T
                            \\
                            F   & F   & T       & F             & T                     & T
                            \\
                            F   & F   & F       & F             & T                     & T
                            \\
                        \end{tabular}
                        }{ \vspace{3cm} }
                        
			\item[d.]   $(p \land \neg q) \to r$
                        \solution{ \\ 
                        \begin{tabular}{c | c | c || c || c || c}
                            $p$ & $q$ & $r$     & $\neg q$  & $p \land \neg q$  & $(p \land \neg q) \to r$ \\ \hline
                            T   & T   & T       & F         & F                 & T
                            \\
                            T   & T   & F       & F         & F                 & T
                            \\
                            T   & F   & T       & T         & T                 & T
                            \\
                            T   & F   & F       & T         & T                 & F
                            \\
                            F   & T   & T       & F         & F                 & T
                            \\
                            F   & T   & F       & F         & F                 & T
                            \\
                            F   & F   & T       & T         & F                 & T
                            \\
                            F   & F   & F       & T         & F                 & T
                            \\
                        \end{tabular}
                        }{ \vspace{4cm} }
                        
		\end{itemize}
    \end{questionNOGRADE}
    
    
    %\newpage
    %\begin{answer}
    %\section*{1.3: Implications (Conditional Propositions) - Answer key}
    
    %\begin{enumerate}
        %\item
            %\begin{itemize}
                %\item[a.]   $g$ is ``it glitters'', $o$ is ``it is gold''. ~\\ $g \to o$. (Your variables may be different.)
                %\item[b.]   $l$ is ``you study linguistics'', $c$ is ``you study computer science'', $n$ is ``you get a job in natural language processing.'' ~\\ $(l \land c) \to n$
                %\item[c.]   $m$ is ``you eat meat'', $c$ is ``you eat cheese'', $v$ is ``you are a vegetarian''. ~\\ $(\neg m \land c) \to v$
                %\item[d.]   $m$ is ``you eat meat'', $c$ is ``you eat cheese'', $v$ is ``you are a vegan''. ~\\ $(\neg m \land \neg c) \to v$, or $\neg( m \lor c ) \to v$
            %\end{itemize}
        %\item   \normalsize
            %\begin{itemize}
                %\item[a.]   Negation: $p \land \neg q$ ~\\
                            %The animal has pointy ears and it is not a cat.
                %\item[b.]   Converse: $q \to p$ ~\\
                            %If the animal is a cat, then the animal has pointy ears.
                %\item[c.]   Inverse: $\neg p \to \neg q$ ~\\
                            %If the animal does not have pointy ears, then the animal is not a cat.
                %\item[d.]   Contrapositive: $\neg q \to \neg p$ ~\\
                            %If the animal is not a cat, then it does not have pointy ears.
            %\end{itemize}
        %\newpage
        %\item   \small  
            %\begin{itemize}
                %\item[a.] $p \to q$ ~\\
                            %\begin{tabular}{c c || c}
                                %$p$ & $q$ & $p \to q$ \\ \hline
                                %T & T & T \\
                                %T & F & F \\
                                %F & T & T \\
                                %F & F & T 
                            %\end{tabular}
                            %~\\
                            %False when $p$ is true and $q$ is false.
                %\item[b.] $p \to (q \lor r)$ ~\\
                            %\begin{tabular}{c c c || c || c}
                                %$p$ & $q$ & $r$ & $q \lor r$ & $p \to (q \lor r)$ \\ \hline
                                %T & T & T & T & T \\
                                %T & T & F & T & T \\
                                %T & F & T & T & T \\
                                %T & F & F & F & F \\
                                %F & T & T & T & T \\
                                %F & T & F & T & T \\
                                %F & F & T & T & T \\
                                %F & F & F & F & T 
                            %\end{tabular}
                            %~\\
                            %False when $p$ is true and $q$ and $r$ are both false.
                %\item[c.] $p \to \neg (q \land r)$ ~\\
                            %\begin{tabular}{c c c || c c || c}
                                %$p$ & $q$ & $r$ & $q \land r$ & $\neg(q \land r)$ & $p \to \neg (q \land r)$ \\ \hline
                                %T & T & T & T & F & F \\
                                %T & T & F & F & T & T \\
                                %T & F & T & F & T & T \\
                                %T & F & F & F & T & T \\
                                %F & T & T & T & F & T \\
                                %F & T & F & F & T & T \\
                                %F & F & T & F & T & T \\
                                %F & F & F & F & T & T
                            %\end{tabular}
                            %~\\
                            %False when $p$ is true and $q$ and $r$ are both true.
                %\item[d.] $(p \land \neg q) \to r$ ~\\
                            %\begin{tabular}{c c c || c c || c}
                                %$p$ & $q$ & $r$ & $\neg q$ & $(p \land \neg q)$ & $(p \land \neg q) \to r$ \\ \hline
                                %T & T & T & F & F & T \\
                                %T & T & F & F & F & T \\
                                %T & F & T & T & T & T \\
                                %T & F & F & T & T & F \\
                                %F & T & T & F & F & T \\
                                %F & T & F & F & F & T \\
                                %F & F & T & T & F & T \\
                                %F & F & F & T & F & T 
                            %\end{tabular}
                            %~\\
                            %False when $p$ is true, $q$ is false, and $r$ is false.
            %\end{itemize}
    %\end{enumerate}
    %\end{answer}
    
\input{../BASE-4-FOOT}
